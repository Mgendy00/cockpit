<?php

/**
 * phpgmi library
 *
 * This file is part of the Pollux.casa cockpit
 * 
 * Source is available here: https://codeberg.org/pollux.casa/cockpit
 * 
 * @license	GNU/AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 * 			see joined LICENSE file
 * 
 * @author	Adële <gemini://adele.work/>
 * 
 **/

if( !defined('PHPGMI_FILE_FINGERPRINTS') ||		// '/home/{user}/fingerprints.txt'
	!defined('PHPGMI_URL_CAPSULE') ||			// 'gemini://{user}.domain.tld' or 'gemini://domain.tld/~{user}'
	!defined('PHPGMI_DIR_CAPSULE') ||			// '/home/{user}/public_gemini'
	!defined('PHPGMI_DIR_WORK') ||				// '/home/{user}/.working'
	!defined('PHPGMI_DIR_COMMON') ||			// '/var/run/phpgmi/common'
	!defined('PHPGMI_DIR_SESSIONS') ||			// '/var/run/phpgmi/phpgmi_sessions'
	!defined('PHPGMI_LIFETIME_SESSIONS') ||		// 7200  (time in seconds)
	!defined('PHPGMI_FINGERPRINT') ||			// isset($_SERVER['TLS_CLIENT_HASH'])?$_SERVER['TLS_CLIENT_HASH']:''
	!defined('PHPGMI_GEMINI_URL')				// $_SERVER['GEMINI_URL']
)
{
	die("50 PHPGMI_* constants not defined\r\n");
}

abstract class phpgmiPage
{
	private 	$m_path='';						// requested uri path (without parameters)
	private 	$m_params=array();				// uri parameters
	private 	$m_input='';					// user input
	private 	$m_user='';						// user associated to client certificate
	private 	$m_response_code=0;				// two digits response code
	private 	$m_response_meta='';			// mime type of the response
	private 	$m_response_lang='';			// lang of the response content
	private 	$m_session_file='';				// session file name
	private		$m_session=false;				// session data

	protected 	$m_lines=array();				// content lines
	protected 	$m_binary=false;				// content if binary response

	static private $s_default_metas = array(
		10 => "Input",
		11 => "Password",
		20 => "text/gemini",
		30 => "./", // redirect temporary
		31 => "/", // redirect permanent
		40 => "Temporary failure",
		41 => "Server unavailable",
		42 => "CGI error",
		43 => "Proxy error",
		44 => "Slow down",
		50 => "Permanent failure",
		51 => "Not found",
		52 => "Gone",
		59 => "Bad request",
		60 => "Client certificate required",
		61 => "Certificate not authorised",
		62 => "Certificate not valid"
	);

	/**
	 * init method is called at page launch and must be defined by child class
	 * In this method, you have to fix the level of authentication :
	 * 		- nothing for public page (disables the use of user interaction)
	 * 		- requireCertificate() : permits session operations
	 * 		- requireAuth() : permits to use working data and access to capsule content
	 */
	abstract function init();

	/**
	 * actionDone method is called when the geminaut has done something on a page
	 *
	 * @param $action is type of action : input / delete / other
	 * @param $var is the name of the variable
	 * @param $value is the value of the variable
	 * @param $parameters is an array of parameters passed with "other" action request
	 */
	abstract function actionDone($action, $name, $value, $parameters);

	/**
	 * build method is called after page rendering
	 * generate the content of your page
	 */
	abstract function build();

	/**
	 * save method is automatically called after page rendering
	 * It's the place to save your working data
	 */
	abstract function save();



	/**
	 * Page constructor
	 *
	 * @param $debug true to enable debug (sometimes useful but break many things)
	 */
	public function __construct($debug=false)
	{
		if($debug)
		{
			echo "20 text/gemini\r\n> Debug mode\r\n```\r\n";
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
		}
		if(!empty(PHPGMI_GEMINI_URL))
		{
			$parsed = parse_url(PHPGMI_GEMINI_URL);
			if($parsed!==false)
			{
				$this->m_path = $parsed['path'];
				if(!empty($parsed['query']))
				{
					parse_str($parsed['query'], $this->m_params);
					$this->m_input = rawurldecode($parsed['query']);
				}
			}
		}
		$this->_findUser();
		$this->init(); // inner call actionDone()
		if(!empty($this->m_session_file))
			$this->_processWaitingActions();
		$this->build();
		$this->render(); // inner call save()
	}

	/**
	 * Find if used client certification belong to a user
	 */
	private function _findUser()
	{
		if(!empty(PHPGMI_FINGERPRINT))
		{
			$rFingerprints = array();
			if(strpos(PHPGMI_FILE_FINGERPRINTS, '{user}')===false) // mono-user config
			{
				$rFingerprints['user'] = PHPGMI_FILE_FINGERPRINTS;
			}
			else  // multi-user config
			{
				$homeroot = substr(PHPGMI_FILE_FINGERPRINTS, 0, strpos(PHPGMI_FILE_FINGERPRINTS, '{user}'));
				$d = dir($homeroot);
				while( false !== ($user = $d->read()) )
				{
					if( substr($user, 0, 1)=='.' )
						continue;
					if( !is_dir($homeroot.$user) )
						continue;
					$fingerprints = str_replace('{user}', $user, PHPGMI_FILE_FINGERPRINTS);
					if(file_exists($fingerprints))
					{
						$rFingerprints[$user] = $fingerprints;
					}
				}
			}

			foreach($rFingerprints as $user => $fingerfile)
			{
				$fingerprints = @file_get_contents($fingerfile);
				foreach(explode("\n",$fingerprints) as $fingerprint)
				{
					if(strcasecmp(trim($fingerprint), PHPGMI_FINGERPRINT)==0)
					{
						$this->m_user = $user;
						$this->m_capsule = str_replace('{user}', $user, PHPGMI_DIR_CAPSULE);
						$this->m_capsule = str_replace('{user}', $user, PHPGMI_DIR_CAPSULE);
						$cap = $this->m_capsule;
						$reserved_folder = dirname($fingerfile)."/reserved/";
						break;
					}
				}
			}
		}
	}


	private function _processWaitingActions()
	{
		if(empty(PHPGMI_FINGERPRINT))
			return;

		$session = $this->getSession();

		// grab new requested session value
		if(isset($session["_phpgmi_nsv_"]))
		{
			$this->requireSessionValue('___'.$session["_phpgmi_nsv_"], $session["_phpgmi_p_"]);
			$name = $session["_phpgmi_nsv_"];
			$params = $session["_phpgmi_params_"];
			$value = $session['___'.$session["_phpgmi_nsv_"]];
			unset($session["_phpgmi_nsv_"]);
			unset($session["_phpgmi_p_"]);
			unset($session["_phpgmi_params_"]);
			unset($session['___'.$name]);
			$this->saveSession($session);
			$this->actionDone('input', $name, $value, $params);
		}

		// check phpgmi action
		if(isset($this->m_params['phpgmi']))
		{
			if($this->m_params['phpgmi'] == 'rd' && !empty($this->m_params['name']) ) // delete value
			{
				$name = $this->m_params['name'];
				$value = isset($this->m_params['value'])?$this->m_params['value']:false;
				$params = isset($this->m_params['params'])?json_decode($this->m_params['params'], true):false;
				unset($this->m_params['phpgmi']);
				unset($this->m_params['name']);
				unset($this->m_params['value']);
				unset($this->m_params['params']);
				$this->actionDone('delete', $name, $value, $params);
				$this->reload();
			}
			elseif($this->m_params['phpgmi'] == 'rc' && !empty($this->m_params['name']) ) // command
			{
				$name = $this->m_params['name'];
				$value = isset($this->m_params['value'])?$this->m_params['value']:false;
				$params = isset($this->m_params['params'])?json_decode($this->m_params['params'], true):false;
				unset($this->m_params['phpgmi']);
				unset($this->m_params['name']);
				unset($this->m_params['params']);
				$this->actionDone('command', $name, $value, $params);
				$this->reload();
			}
			elseif($this->m_params['phpgmi'] == 'ri' && !empty($this->m_params['name']) ) // Request new value input
			{
				$name = $this->m_params['name'];
				$prompt = empty($this->m_params['p']) ? ('Enter '.$name) : $this->m_params['p'];
				$params = isset($this->m_params['params'])?json_decode($this->m_params['params'], true):false;
				if(isset($session['___'.$name]))
				{
					unset($session['___'.$name]);
				}
				$session["_phpgmi_nsv_"] = $name;
				$session["_phpgmi_p_"] = $prompt;
				$session["_phpgmi_params_"] = $params;
				$this->saveSession($session);
				$this->reload();
			}
		}
	}

	/**
	 * Ask geminaut for a client certificate if none receiver
	 */
	public function requireCertificate()
	{
		if(empty(PHPGMI_FINGERPRINT))
			$this->renderMeta(60);

		$this->m_session_file = PHPGMI_DIR_SESSIONS.'/'.str_replace(':', '-', PHPGMI_FINGERPRINT).'.ser';
	}

	/**
	 * Ask geminaut for a session value if not filled
	 *
	 */
	public function requireSessionValue($name, $prompt=false, $sensitive=false)
	{
		if(empty(PHPGMI_FINGERPRINT))
			$this->renderMeta(60);
		$sensitive = intval($sensitive!==false);
		$session = $this->getSession();
		if($prompt===false)
			$prompt = "Enter ".$name;
		if(empty($session[$name]))
		{
			$input=$this->getInput();
			if(empty($input))
				$this->renderMeta(10 + $sensitive, $prompt);
			$session[$name] = $input;
			$this->saveSession($session);
			$this->renderMeta(30,$this->m_path);
		}

		if(empty(PHPGMI_FINGERPRINT))
			$this->renderMeta(60);

		//$this->m_session_file = PHPGMI_DIR_SESSIONS.'/'.str_replace(':', '-', PHPGMI_FINGERPRINT).'.ser';
	}


	/**
	 * Ask geminaut for a known client certificate if not found
	 */
	public function requireAuth()
	{
		$this->requireCertificate();

		if(!empty($this->m_user))
			return;

		if(strpos(PHPGMI_FILE_FINGERPRINTS, '{user}')===false) // mono-user config
			$fingerfile = PHPGMI_FILE_FINGERPRINTS;
		else  // multi-user config
			$fingerfile = '~'.substr(PHPGMI_FILE_FINGERPRINTS, strpos(PHPGMI_FILE_FINGERPRINTS, '{user}')+6);

		$this->add("## Unknown certificate");
		$this->add("Please, open your ".$fingerfile." file on the server and add this fingerprint:");
		$this->add("``` certificate fingerprint");
		$this->add(PHPGMI_FINGERPRINT);
		$this->add("```");
		$this->render();
	}

	/**
	 * Get capsule url if user is authenticated
	 *
	 * @return capsule root url
	 */
	public function getCapsuleUrl()
	{
		if(!empty($this->m_user))
			return str_replace('{user}', $this->m_user, PHPGMI_URL_CAPSULE);
		return false;
	}

	/**
	 * Get capsule dir if user is authenticated
	 *
	 * @return capsule full path
	 */
	public function getCapsulePath()
	{
		if(!empty($this->m_user))
			return str_replace('{user}', $this->m_user, PHPGMI_DIR_CAPSULE);
		return false;
	}

	/**
	 * Get work dir if user is authenticated
	 *
	 * @return work directory full path
	 */
	public function getWorkPath()
	{
		if(!empty($this->m_user))
			return rtrim(str_replace('{user}', $this->m_user, PHPGMI_DIR_WORK), '/');
		return false;
	}

	/**
	 * get session data
	 *
	 * @return session array
	 */
	public function getSession()
	{
		if(is_array($this->m_session))
			return $this->m_session;

		if(empty($this->m_session_file))
			return false;

		$ser = @file_get_contents($this->m_session_file);
		if(empty($ser))
			$this->m_session = array();
		else
			$this->m_session = unserialize($ser);

		return $this->m_session;
	}


	/**
	 * Save array session data
	 */
	public function saveSession($session)
	{
		if(empty(PHPGMI_FINGERPRINT))
			return false;
		$this->m_session = $session;
		@mkdir(PHPGMI_DIR_SESSIONS);
		return file_put_contents($this->m_session_file, serialize($this->m_session));
	}


	/**
	 * get user input
	 *
	 * @return user input text
	 */
	public function getInput()
	{
		return $this->m_input;
	}

	/**
	 * get Url parameters
	 *
	 * @return array of input parameters
	 */
	public function getUrlParams()
	{
		return $this->m_params;
	}
	
	/**
	 * get Url parameter
	 *
	 * @param $param name
	 * @return value or false if not set
	 */
	public function getUrlParam($param)
	{
		if(isset($this->m_params[$param]))
			return $this->m_params[$param];
		return false;
	}

	/**
	 * get user
	 *
	 * @return user account
	 */
	function getUser()
	{
		return $this->m_user;
	}


	/**
	 * get working data
	 *
	 * @param $section : section name of data (working data can be seperated in different section)
	 *
	 * @return working data array
	 */
	public function getWorkingData($section)
	{
		$this->requireAuth();

		$data_file = $this->getWorkPath().'/'.$section.'.ser';
		$ser = @file_get_contents($data_file);
		if(empty($ser))
			$data = array();
		else
			$data = unserialize($ser);

		return $data;
	}


	/**
	 * Save array working data
	 *
	 * @param $section : section name of data (working data can be seperated in different section)
	 */
	public function saveWorkingData($section, $data)
	{
		if(empty($this->m_user))
			return false;

		$data_file = $this->getWorkPath().'/'.$section.'.ser';
		@mkdir(dirname($data_file));
		return file_put_contents($data_file, serialize($data));
	}

	/**
	 * Get external Gemini content
	 * 
	 * @param $uri of the external resource
	 * @param &$meta to get return status (first line of response) 
	 * @param $redir_count is a security to infinite redirection (0 for first call)
	 *  
	 * @return content
	 */
	public function getGeminiContent($uri, &$meta, $redir_count=0)
	{
		if($redir_count>5)
		{
			$meta="40 Too many redirections";
			return false;
		}
		$uricomp = parse_url($uri);
		if($uricomp['scheme']!='gemini')
		{
			$meta="43 Bad protocol ".$uricomp['scheme'];
			return false;
		}
		if(empty($uricomp['host']))
		{
			$meta="59 Bad request";
			return false;
		}
		
		$context = stream_context_create();
		stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
		stream_context_set_option($context, 'ssl', 'verify_peer', false);
		stream_context_set_option($context, 'ssl', 'verify_peer_name', false);

		if(empty($uricomp['port']))
			$uricomp['port'] = 1965;

		$fp = stream_socket_client('tls://'.$uricomp['host'].':'.$uricomp['port'], $errno, $errstr, 10, STREAM_CLIENT_CONNECT, $context);
		if($fp===false)
		{
			$meta="41 Server timeout ($errno : $errstr)";
			return false;
		}
		
		fputs($fp, $uri."\r\n");

		$meta = fgets($fp, 1024);
		if(empty($meta))
		{
			$meta="52 Server gone";
			return false;
		}
		
		if(substr($meta,0,1) == '3') // redirect
		{
			fclose($fp);
			$redir = substr($meta,3);
			if(strpos($redir,'://')===false) // relative
			{
				if(substr($redir,0,1)=='/')
					$redir = 'gemini://'.$uricomp['host'].($uricomp['port']==1965?'':(':'.$uricomp['port'])).$redir;
				else
					$redir = 'gemini://'.$uricomp['host'].($uricomp['port']==1965?'':(':'.$uricomp['port'])).dirname($uricomp['path']).'/'.$redir;
			}
			$meta='';
			return $this->getGeminiContent($redir, $meta, $redir_count+1);
		}
		
		$content=false;
		while (!feof($fp)) {
			$content.=fread($fp, 1024);
		}
		fclose($fp);
		return $content;
	}


	/**
	 * get certificate fingerprint
	 *
	 * @return certificate fingerprint
	 */
	function getFingerprint()
	{
		return PHPGMI_FINGERPRINT;
	}

	/**
	 * Add line to content
	 *
	 * @param string $line without CR/LF
	 */
	public function add($line)
	{
		$this->m_lines[] = $line;
	}


	/**
	 * Add preformatted block to content
	 *
	 * @param $block destination uri
	 * @param $title of the block, use as alternative for accessibility
	 */
	public function addPreformatted($block, $title="")
	{
		$this->add("``` ".$title);
		foreach(explode("\n", $block) as $line)
			$this->add($line);
		$this->add("```");
	}


	/**
	 * Add blockquote to content
	 *
	 * @param $blockquote
	 */
	public function addBlockquote($blockquote)
	{
		foreach(explode("\n", $blockquote) as $line)
			$this->add("> ".$line);
	}


	/**
	 * Add link line to content
	 *
	 * @param $uri destination uri
	 * @param $linkText text of the link
	 */
	public function addLink($uri, $linkText)
	{
		$this->add("=> ".$uri." ".$linkText);
	}


	/**
	 * Add line to content creating a link to remove a session value
	 * 
	 * it calls back actionDone('delete', $name, $value, $parameters)
	 *
	 * @param $name of value
	 * @param $value to delete (could be key, index, id...) and empty
	 * @param $linkText text of the link
	 * @param $parameters array of optional data to passthru to action command
	 */
	public function addLinkRequestDelete($name, $value, $linkText, $parameters=false)
	{
		$uri = PHPGMI_GEMINI_URL;
		if($i=strpos($uri,'?'))
			$uri = substr($uri, 0, $i);
		$uri .= "?cmt=***Deleting...***&phpgmi=rd&name=".rawurlencode($name)."&value=".rawurlencode($value)."&params=".rawurlencode(json_encode($parameters))."&t=".microtime(true);
		$this->add("=> ".$uri." ".$linkText);
	}


	/**
	 * Add line to content creating a link to request a user input
	 *
	 * it calls back actionDone('input', $name, $value, $parameters)
	 *
	 * @param $name of input
	 * @param $linkText text of the link
	 * @param $prompt text of prompt (if empty, linkText is used
	 * @param $parameters array of optional data to passthru to action command
	 */
	public function addLinkRequestInput($name, $linkText, $prompt='', $parameters=false)
	{
		$uri = PHPGMI_GEMINI_URL;
		if($i=strpos($uri,'?'))
			$uri = substr($uri, 0, $i);
		if(empty($prompt))
			$prompt=$linkText;
		$uri .= "?cmt=***Input...***&phpgmi=ri&name=".rawurlencode($name)."&params=".rawurlencode(json_encode($parameters))."&p=".rawurlencode($prompt)."&t=".microtime(true);
		$this->add("=> ".$uri." ".$linkText);
	}

	/**
	 * Add line to content creating a link to request a user command
	 *
	 * it calls back actionDone('command', $name, $value, $parameters)
	 * 
	 * @param $name of value
	 * @param $value to apply command (could be key, index, id...) and empty
	 * @param $linkText text of the link
	 * @param $parameters array of optional data to passthru to action command
	 */
	public function addLinkRequestCommand($name, $value, $linkText, $parameters=false)
	{
		$uri = PHPGMI_GEMINI_URL;
		if($i=strpos($uri,'?'))
			$uri = substr($uri, 0, $i);
		$uri .= "?cmt=***Please-wait...***&phpgmi=rc&name=".rawurlencode($name)."&value=".rawurlencode($value)."&params=".rawurlencode(json_encode($parameters))."&t=".microtime(true);
		$this->add("=> ".$uri." ".$linkText);
	}


	/**
	 * Prepare binary response
	 *
	 * @param $binary = binary content
	 * @param $mimetype = mime type of this content
	 */
	public function setBinaryContent($binary, $mimetype="application/octet-stream")
	{
		$this->m_binary = $binary;
		$this->m_response_meta = $mimetype;
	}


	/**
	 * Prepare binary response
	 *
	 * @param $filename = binary content
	 * @param $mimetype = mime type of this content or false for autodetect
	 *
	 * @return false if file not found
	 */
	public function setBinaryFile($filename, $mimetype=false)
	{

		$this->m_binary = @file_get_contents($filename);
		if($this->m_binary===false)
			return false;

		$this->m_response_meta = $mimetype;

		if($mimetype===false)
			$mimetype = @mime_content_type($filename);

		if($mimetype===false)
			$mimetype = "application/octet-stream";

		return true;
	}


	/**
	 * Reload page without params
	 *
	 */
	public function reload()
	{
		$uri = PHPGMI_GEMINI_URL;
		if(($i=strpos($uri,'?'))!==false)
			$uri = substr($uri, 0, $i);
		$this->renderMeta(30, $uri);
	}

	/**
	 * Redirect to a new uri
	 *
	 * @param $uri destination
	 */
	public function redirect($uri)
	{
		$this->renderMeta(30, $uri);
	}

	/**
	 * Render with a specific code and meta
	 *
	 * @param $code 2 digit response code
	 * @param $meta Meta response (or false for default)
	 */
	public function renderMeta($code, $meta=false)
	{
		if(!empty(PHPGMI_FINGERPRINT))
			$this->save();

		if($meta == false && isset(self::$s_default_metas[$code]))
			$meta = self::$s_default_metas[$code];
		echo $code.' '.$meta."\r\n";
		exit();
	}


	/**
	 * Render meta and content
	 */
	public function render()
	{
		if(!empty(PHPGMI_FINGERPRINT))
			$this->save();

		if(!$this->m_response_code)
			$this->m_response_code = 20;
		if(empty($this->m_response_meta))
			$this->m_response_meta = 'text/gemini';
		if($this->m_response_meta=='text/gemini' && !empty($this->m_response_lang))
			$this->m_response_meta .= '; lang='.$this->m_response_lang;

		// header
		echo $this->m_response_code.' '.$this->m_response_meta."\r\n";

		if(count($this->m_lines)>0) // lines content
		{
			foreach($this->m_lines as $line)
				echo $line."\r\n";
		}
		else // binary content
		{
			echo $this->m_binary;
		}
		exit();
	}
}
